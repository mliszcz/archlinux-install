# archlinux-install
My personalized Arch Linux installation.

## Installation steps (archiso)

First clone this repository:
```bash
pacman -Sy git
git clone https://gitlab.com/mliszcz/archlinux-install
```

Then run the installation scripts:
```bash
./archlinux-install/install-livecd.sh /dev/sda 8G
arch-chroot /mnt sh /root/archlinux-install/install-chroot.sh /dev/sda pinky
```

Finally, clean-up (this is optional) and reboot:
```bash
swapoff /mnt/swapfile
umount -R /mnt
cryptsetup close root
reboot
```

There are some system-wide dotfiles in the `etc` directory, notably a pacman
hook that maintains a list of installed packages. All those files may be
symlinked from this repository once the system is rebooted:
```bash
cd /root/archlinux-install
cp -as "$PWD/etc/." /etc
pacman -Sy --needed - < etc/pkglist.txt
```

The users can be added now:
```bash
useradd -m -G wheel,docker mliszcz
```

## Tracking installed packages

The `packages/` directory contains files with lists of packages. To verify
what packages are currently installed and to detect any untracked packages
the `packages-list-diff.py` script can be used:
```bash
./packages-list-diff.py packages/*
```

## Unlocking disk without passphrase

The root partition is encrypted with dm-crypt. To log in without providing a
passphrase, create a key file and put it on the boot partition (*unsafe*) or
on a removable USB drive.
```bash
openssl rand -base64 12 > /boot/encryption.keyfile
cryptsetup luksAddKey /dev/sdb2 /boot/encryption.keyfile
```

The kernel commandline must be updated and any modules required for mounting
the keydev (e.g. vfat) must be included in the initramfs.
```
rd.luks.name=XXX=root
rd.luks.key=XXX=/encryption.keyfile:UUID=YYY
rd.luks.options=XXX=keyfile-timeout=10s
root=UUID=ZZZ
```

The module for the corresponding filesystem may need to be added to initramfs.
For example: `MODULES=(vfat)` in `/etc/mkinitcpio.conf`.

## Working with this repository

After the installation this repository will be owned by root. To push commits
to the remote configure git as follows:

```bash
git config --local user.name mliszcz
git config --local user.email XXX
git config --local core.sshCommand "ssh -i /home/mliszcz/.ssh/id_ed25519"
git remote set-url origin git@gitlab.com:mliszcz/archlinux-install
```
