variable "cache_dir" {
  default = "${env("HOME")}/.cache/packer"
}

locals {
  iso_url           = "https://geo.mirror.pkgbuild.com/iso/2023.01.01/archlinux-2023.01.01-x86_64.iso"
  iso_checksum      = "sha256:61dbae312cf677be38a93f424c91abadd8a8ed1f3a602b697aac4c57a7872645"
  output_directory  = "build"
  vm_name           = "packer-arch"
}

source "qemu" "arch" {
  iso_url           = local.iso_url
  iso_checksum      = local.iso_checksum
  output_directory  = local.output_directory
  vm_name           = local.vm_name
  skip_compaction   = true
  memory            = 1024 # 1G of RAM is required to boot the live iso.
  disk_size         = "10G"
  net_device        = "virtio-net-pci"
  disk_interface    = "virtio-scsi"
  ssh_username      = "root"
  ssh_password      = "admin"
  ssh_timeout       = "120m"
  shutdown_command  = "poweroff"
  shutdown_timeout  = "30s"
  boot_wait         = "10s"
  boot_steps = [
    ["<enter><wait90>", "Select bootloader entry"],
    ["systemctl stop sshd<enter><wait5>", "Stop sshd"],
    ["echo -e 'admin\\nadmin' | passwd<enter><wait5>", "Set root password"],
    ["echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config<enter><wait5>", "Allow root SSH login"],
    ["systemctl start sshd<enter><wait5>", "Restart sshd"],
  ]
  qemuargs = [
    ["-cpu", "host"],
    ["-smp", "2"],
    ["-monitor", "unix:/tmp/${local.vm_name}.monitor.sock,server=on,wait=off"],
    # OVMF_VARS is a local copy of /usr/share/edk2-ovmf/x64/OVMF_VARS.fd.
    # https://wiki.archlinux.org/title/QEMU#Running_virtualized_system.
    ["-drive", "if=pflash,format=raw,readonly=on,file=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd"],
    ["-drive", "if=pflash,format=raw,file=./OVMF_VARS.fd"],
    # The CDROM and the main disk need to be readded manually because the above
    # -drive option (pflash) overrides all other (also default) -drive options. 
    # See the below projects for details how the file locations are determined:
    # (packer-plugin-qemu): builder/qemu/step_create_disk.go
    # (packer-plugin-sdk): multistep/commonsteps/step_download.go, packer/cache.go
    ["-drive", "if=none,file=${local.output_directory}/${local.vm_name},id=drive0,cache=writeback,discard=ignore,format=qcow2"],
    ["-drive", "file=${var.cache_dir}/${sha1(local.iso_checksum)}.iso,media=cdrom"],
  ]
}

build {
  sources = ["source.qemu.arch"]
  # Copy the (possibly modified) working tree into the VM. Normally we would
  # clone the repository with the shell provisioner but that would not allow
  # to test the local changes before commiting them to the remote repository.
  provisioner "file" {
    # Using 'path.cwd' requires running the build from the repository root.
    # The only other option is 'path.root' but it is '.' (dot) when packer
    # is called like 'packer build .' and scp can not copy such directory.
    # https://superuser.com/questions/1536087/scp-the-current-dir-to-another-name
    source = "${path.cwd}"
    destination = "/root"
  }
  provisioner "shell" {
    inline = [
      "set -e",
      "set -v",
      "pacman -Sy --noconfirm git expect",
      # Below directory is provided by the previous provisioner.
      "cd archlinux-install",
      "expect -c 'spawn ./install-livecd.sh /dev/sda 100M' expect-livecd.exp secret",
      # Use the same password as in livecd to keep the ssh communicator.
      "expect -c 'spawn arch-chroot /mnt sh /root/archlinux-install/install-chroot.sh /dev/sda archvm' expect-chroot.exp admin",
      # Install and enable ssh and allow root to login. This is not part of
      # the installation procedure but is needed for further provisioning.
      "arch-chroot /mnt pacman -Sy --needed --noconfirm openssh",
      "arch-chroot /mnt systemctl enable sshd",
      "echo 'PermitRootLogin yes' >> /mnt/etc/ssh/sshd_config",
      # Clean up after the installation.
      "swapoff /mnt/swapfile",
      "umount -R /mnt",
      "cryptsetup close root",
      # Shut down the network connection before reboot. This is recommended
      # according to the documentation but it seems that it is not needed.
      # "ip link set $(ip -o -4 route show to default | awk '{print $5}') down",
      # Schedule a reboot in 1 minute. We can not just call 'reboot' from
      # the script as it will block the provisioner (expect_disconnect does
      # not work, see also https://github.com/hashicorp/packer/issues/11190).
      # Instead, this script will end right now and the next script (local)
      # will wait for a reboot before typing the disc encryption password.
      "shutdown --reboot +1",
    ]
  }
  provisioner "shell-local" {
    inline = [
      "set -e",
      "set -v",
      "sleep 80",
      "res=; for x in s e c r e t ret; do res+=\"sendkey $x\n\"; done;",
      "echo -e \"$res\" | socat - unix-connect:/tmp/${local.vm_name}.monitor.sock",
    ]
  }
  provisioner "shell" {
    inline = [
      "set -e",
      "set -v",
      "cd /root/archlinux-install",
      "cp -as \"$PWD/etc/.\" /etc",
      "pacman -Sy --needed --noconfirm - < etc/pkglist.txt",
      "journalctl --priority warning",
    ]
  }
}
