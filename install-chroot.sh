#!/bin/sh
#
# Arch Linux installation script (chroot part).
# See: https://wiki.archlinux.org/index.php/instalation_guide#Configure_the_system
#

if [ "$#" -ne 2 ]; then
  echo "usage: $0 <disk device> <hostname>"
  echo "example: $0 /dev/sda pinky"
  exit 1
fi

set -e
set -v

DISK_DEV=$1  # e.g. /dev/sda
HOST=$2 # e.g. pinky

DISK_BOOT_DEV=${DISK_DEV}1
DISK_ROOT_DEV=${DISK_DEV}2

if [ ! -b $DISK_BOOT_DEV ]; then
  DISK_BOOT_DEV=${DISK_DEV}p1
  DISK_ROOT_DEV=${DISK_DEV}p2
fi

ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime

hwclock --systohc --utc

sed -i -E 's/^#(pl_PL.UTF-8.*$)/\1/g' /etc/locale.gen
sed -i -E 's/^#(en_US.UTF-8.*$)/\1/g' /etc/locale.gen
locale-gen

cat << EOF > /etc/locale.conf
LANG=pl_PL.UTF-8
LANGUAGE=en_US.UTF-8
LC_COLLATE=C
LC_CTYPE=en_US.UTF-8
LC_MESSAGES=en_US.UTF-8
EOF

cat << EOF > /etc/vconsole.conf
KEYMAP=pl
FONT=lat2-16
FONT_MAP=8859-2
EOF

echo $HOST > /etc/hostname

# We will have systemd-based initramfs. For LUKS/dm-crypt we need
# following hooks: systemd, keyboard, sd-vconsole and sd-encrypt.
# See: https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LUKS_on_a_partition
HOOKS='HOOKS="base systemd autodetect keyboard sd-vconsole modconf block sd-encrypt filesystems"'
sed -i -E "s/^HOOKS=.*$/#\0\n$HOOKS/g" /etc/mkinitcpio.conf
mkinitcpio -P

passwd

bootctl install

cat << EOF > /boot/loader/loader.conf
default arch.conf
timeout 3
editor  no
EOF

# Normally kernel options should include root= but since we are using systemd
# discoverable partitions, the root device will be automatically detected and
# decrypted (there is no need to specify rd.luks.uuid as well). We still need
# to provide resume= and resume_offset= to support hibernation into swapfile.
# resume_offset= is the physical offset of the first extent in the swap file.
# If needed, UUIDs can be checked with the following command: > lsblk -o +UUID
# See: https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate#Hibernation_into_swap_file
LUKS_UUID=`blkid -s UUID -o value ${DISK_ROOT_DEV}`
ROOT_UUID=`blkid -s UUID -o value /dev/mapper/root`
SWAPFILE_OFFSET=`filefrag -v /swapfile | awk '{if($1=="0:") print $4}' | grep -Po '\d+'`
cat << EOF > /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /amd-ucode.img
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options rw resume=/dev/mapper/root resume_offset=$SWAPFILE_OFFSET
# options rw rd.luks.name=$LUKS_UUID=root root=UUID=$ROOT_UUID resume=UUID=$ROOT_UUID resume_offset=$SWAPFILE_OFFSET
EOF

# Memtest requires memtest86+-efi package.
cat << EOF > /boot/loader/entries/memtest.conf
title Memtest86+
efi   /memtest86+/memtest.efi
EOF

# Enable network services so that they are available right after reboot.
systemctl enable NetworkManager
systemctl enable systemd-timesyncd
