#!/bin/sh
#
# Arch Linux installation script (archiso part).
# See: https://wiki.archlinux.org/index.php/instalation_guide#Pre-installation
# See: https://wiki.archlinux.org/index.php/instalation_guide#Installation
#

if [ "$#" -ne 2 ]; then
  echo "usage: $0 <disk device> <swap size>"
  echo "example: $0 /dev/sda 8G"
  exit 1
fi

set -e
set -v

DISK_DEV=$1  # e.g. /dev/sda
SIZE_SWAP=$2 # e.g. 8G

loadkeys pl

# Ensure that the network is operational.
ping -c 1 -W 1 archlinux.org

timedatectl status

parted $DISK_DEV mklabel gpt
parted $DISK_DEV mkpart ESP 0% 512MiB
parted $DISK_DEV mkpart primary 512MiB 100%
parted $DISK_DEV set 1 esp on

# We will use systemd's Discoverable Partitions Specifications.
# parted correctly sets partition type GUID for ESP but leaves
# root partition type GUID set to 'Other Data Partitions'.
# See: https://uapi-group.org/specifications/specs/discoverable_partitions_specification/
# Partition types can be checked with the following command:
# > lsblk -o +PARTTYPE

sfdisk --part-type $DISK_DEV 2 4f68bce3-e8cd-4db1-96e7-fbcaf984b709

DISK_BOOT_DEV=${DISK_DEV}1
DISK_ROOT_DEV=${DISK_DEV}2

# NVMe device will use name /dev/nvme0n1pX instead of /dev/sdaX.
if [ ! -b $DISK_BOOT_DEV ]; then
  DISK_BOOT_DEV=${DISK_DEV}p1
  DISK_ROOT_DEV=${DISK_DEV}p2
fi

# The second partition will contain LUKS encrypted rootfs.
# See: https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LUKS_on_a_partition
cryptsetup -v --use-random luksFormat $DISK_ROOT_DEV
cryptsetup open $DISK_ROOT_DEV root

mkfs.fat -F32 $DISK_BOOT_DEV
mkfs.ext4 /dev/mapper/root

mount /dev/mapper/root /mnt

# systemd will automatically mount ESP to either /efi or /boot
# (depending on which directory exists in the root file system).
# As systemd-boot cannot access files located outside of ESP, we 
# must ensure that kernel, initramfs and microcode image will be
# stored on the ESP, thus we must mount ESP to /boot.
# See: https://wiki.archlinux.org/index.php/Arch_boot_process#Boot_loader
# See: https://wiki.archlinux.org/index.php/EFI_system_partition#Typical_mount_points
mkdir /mnt/boot
mount $DISK_BOOT_DEV /mnt/boot

# Since the disk is encrypted and we do not want LVM, we must use swap file.
# See: https://wiki.archlinux.org/index.php/swap#Swap_file
SIZE_SWAP_MB=`numfmt --from=iec --to-unit=1Mi $SIZE_SWAP`
dd if=/dev/zero of=/mnt/swapfile bs=1M count=$SIZE_SWAP_MB status=progress
chmod 600 /mnt/swapfile
mkswap /mnt/swapfile
swapon /mnt/swapfile

# It is safe to install both AMD and Intel microcode images for portabililty.
# cryptsetup package is required as it provides (sd-)encrypt initramfs hooks.
pacstrap -K /mnt base linux linux-firmware cryptsetup amd-ucode intel-ucode networkmanager

# At this point we should generate fstab. Since we are using discoverable
# partitions, this is not necessary. We just need entry for the swapfile.
echo '/swapfile none swap defaults 0 0' >> /mnt/etc/fstab
genfstab -t UUID /mnt >> /mnt/etc/fstab.reference

# Copy itself to the rootfs to continue the installation.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cp -r $DIR /mnt/root/

# The first part of the installation process is done.
# Now continue with the install-chroot.sh script.
