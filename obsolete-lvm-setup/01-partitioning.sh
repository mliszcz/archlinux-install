#!/usr/bin/env bash

# Partitioning (UEFI + lvm + dm-crypt/LUKS)
#
# https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS
# https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode
# https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate
#
# +------------------+----------+----------+----------+
# |                  | vg0-swap | vg0-root | vg0-home |
# |                  |----------+----------+----------+
# | /boot (ESP)      | Volme group vg0                |
# |                  |--------------------------------+
# |                  | LUKS container                 |
# +------------------+--------------------------------+
# | /dev/sdx1 (8300) | /dev/sdx2 (8E00)               |
# +------------------+--------------------------------+

if [ "$#" -ne 3 ]; then
    echo "usage: $0 <disk device> <swap size> <root size>"
    echo "example: $0 /dev/sda 8G 40G"
    exit 1
fi

set -v

DISK_DEV=$1  # e.g. /dev/sda
SIZE_SWAP=$2 # e.g. 8G
SIZE_ROOT=$3 # e.g. 40G

loadkeys pl
timedatectl set-ntp true

parted $DISK_DEV mklabel gpt
parted $DISK_DEV mkpart ESP 0% 512MiB
parted $DISK_DEV mkpart primary 512MiB 100%
parted $DISK_DEV set 1 esp on
parted $DISK_DEV set 2 lvm on
# parted $DISK_DEV print
# sgdisk -p $DISK_DEV

DISK_BOOT_DEV=${DISK_DEV}1
DISK_ROOT_DEV=${DISK_DEV}2

cryptsetup -v --use-random luksFormat $DISK_ROOT_DEV
cryptsetup open $DISK_ROOT_DEV lvm

pvcreate /dev/mapper/lvm
vgcreate vg0 /dev/mapper/lvm
lvcreate -L $SIZE_SWAP vg0 -n swap
lvcreate -L $SIZE_ROOT vg0 -n root
lvcreate -l 100%FREE   vg0 -n home

mkfs.fat -F32 $DISK_BOOT_DEV
mkfs.ext4 /dev/mapper/vg0-root
mkfs.ext4 /dev/mapper/vg0-home

mkswap /dev/mapper/vg0-swap
swapon /dev/mapper/vg0-swap

mount /dev/mapper/vg0-root /mnt
mkdir /mnt/boot
mount $DISK_BOOT_DEV /mnt/boot
mkdir /mnt/home
mount /dev/mapper/vg0-home /mnt/home

pacstrap /mnt base base-devel cryptsetup lvm2 intel-ucode

genfstab -t UUID /mnt >> /mnt/etc/fstab

# copy itself to rootfs
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cp -r $DIR /mnt/root/

set +v
