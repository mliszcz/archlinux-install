#!/usr/bin/env bash

# Install and configure base system and bootloader
# Initramfs is systemd based. Bootloader is systemd-boot.
#
# http://man7.org/linux/man-pages/man1/systemd-firstboot.1.html
# https://wiki.archlinux.org/index.php/systemd-boot#Configuration
# https://wiki.archlinux.org/index.php/Dm-crypt/System_configuration

# TODO: add ohci_hcd kernel module for sd card support

if [ "$#" -ne 2 ]; then
    echo "usage: $0 <root partition> <hostname>"
    echo "example: $0 /dev/sda2 pinky"
    exit 1
fi

set -v

ln -sf /run/systemd/resolve/resolv.conf /mnt/etc/resolv.conf

ROOT_UUID=`blkid -s UUID -o value $1` # e.g. /dev/sda2
HOST=$2 # e.g. pinky

sed -i -E 's/^#(pl_PL.UTF-8.*$)/\1/g' /etc/locale.gen
sed -i -E 's/^#(en_US.UTF-8.*$)/\1/g' /etc/locale.gen
locale-gen

cat << EOF > /etc/locale.conf
LANG=pl_PL.UTF-8
LANGUAGE=en_US.UTF-8
LC_COLLATE=C
LC_CTYPE=en_US.UTF-8
LC_MESSAGES=en_US.UTF-8
EOF

echo 'KEYMAP=pl' > /etc/vconsole.conf

ln -s /usr/share/zoneinfo/Europe/Warsaw /etc/localtime

echo $HOST > /etc/hostname

hwclock --systohc --utc

# NOTE: remember to set non-empty domain on DHCP server (e.g. router),
# Otherwise systemd-resolved won't resolve single-label names.
# See 'hosts' in /etc/resolv.conf.

cat << EOF > /etc/systemd/network/default.network
[Match]
Name=en*

[Network]
DHCP=ipv4

[DHCP]
UseDomains=true
EOF

systemctl enable systemd-networkd.service
systemctl enable systemd-resolved.service
systemctl enable systemd-timesyncd.service

# TODO: fix warning: Possible missing firmware for module: wd719x; same for: aic94xx
HOOKS='HOOKS="base systemd autodetect keyboard modconf block sd-vconsole sd-encrypt sd-lvm2 filesystems"'
sed -i -E "s/^HOOKS=.*$/#\0\n$HOOKS/g" /etc/mkinitcpio.conf
mkinitcpio -p linux

bootctl --path=/boot install

cat << EOF > /boot/loader/loader.conf
default arch
timeout 0
editor  0
EOF

cat << EOF > /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options rd.luks.uuid=$ROOT_UUID root=/dev/mapper/vg0-root resume=/dev/mapper/vg0-swap rd.luks.options=discard quiet splash rw
EOF

sed -i -E 's/^(\s*issue_discards = ).*$/\11/g' /etc/lvm/lvm.conf
gawk -i inplace '!/^#/ && ($2 != "/boot") { if(!match(/discard/, $4)) $4=$4",discard" } 1' /etc/fstab

sed -i -E 's/^# (%wheel ALL=\(ALL\) ALL.*$)/\1/g' /etc/sudoers

passwd

set +v
