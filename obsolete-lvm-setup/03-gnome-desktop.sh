#!/usr/bin/env bash

# https://wiki.archlinux.org/index.php/Hardware_video_acceleration#Installation

set -v

pacman -Syu --noconfirm \
  xf86-video-intel \
  mesa-libgl \
  libva-intel-driver \
  alsa-utils \
  pulseaudio \
  gstreamer \
  gst-libav \
  gst-plugins-base \
  gst-plugins-good \
  gst-plugins-ugly \
  gst-plugins-bad

pacman -Syu --noconfirm \
  weston \
  xorg-server-xwayland \
  gnome \
  gedit \
  gnome-calendar \
  gnome-clocks \
  gnome-maps \
  gnome-sound-recorder \
  gnome-weather \
  gnome-todo \
  gnome-tweak-tool \
  evolution \
  nautilus-sendto \
  file-roller \
  networkmanager \
  network-manager-applet \
  ttf-freefont \
  ttf-dejavu \
  ttf-hack \
  arc-solid-gtk-theme \
  deepin-icon-theme

pacman -Syu --noconfirm bluez bluez-utils obexftp obextool obexfs

localectl set-x11-keymap pl,us

sed -i -E 's/^#(Color)$/\1/g' /etc/pacman.conf

systemctl enable gdm.service
systemctl enable NetworkManager.service
systemctl enable bluetooth.service

set +v
