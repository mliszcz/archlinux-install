#!/usr/bin/env bash

# https://bbs.archlinux.org/viewtopic.php?id=134109
# https://wiki.archlinux.org/index.php/Intel_graphics#RC6_sleep_modes_.28enable_rc6.29
# http://files.bestmail.ws/Arch/setup/tlp.txt
# https://wiki.archlinux.org/index.php?title=TLP
# https://wiki.archlinux.org/index.php/intel_graphics
# https://wiki.manjaro.org/index.php?title=Optimized_power_settings
# https://forum.manjaro.org/t/howto-power-savings-setup-update-20161006/1445

# acpi_call replaces tm_smapi on SandyBridge (or newer) ThinkPads

# Pass these options to i915 module (either via kernel params or modprobe config)
# pcie_aspm=force i915.semaphores=1 i915.i915_enable_rc6=1 i915.i915_enable_fbc=1 i915.lvds_downclock=1
# Note: some option names have changed.
# "For those using pcie_aspm=force. You might get lower power usage if you remove that entry."

# Check:
# modinfo -p i915
# systool -m i915 -av

set -v

echo "options i915 semaphores=1 enable_rc6=7 enable_fbc=1" > /etc/modprobe.d/i915.conf

pacman -Syu acpi_call tlp

systemctl disable systemd-rfkill.service
systemctl enable tlp.service
systemctl enable tlp-sleep.service

set +v
