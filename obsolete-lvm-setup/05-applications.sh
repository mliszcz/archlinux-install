#!/usr/bin/env bash

set -v

pacman -Syu --noconfirm \
  openssh \
  git \
  vim \
  neovim \
  tmux \
  tree \
  wget \
  unrar \
  parallel \
  fzf \
  exa \
  ripgrep fd \
  highlight \
  pygmentize \
  openbsd-netcat \
  strace \
  parted \
  stow \
  dosfstools \
  ntfsprogs \
  bind-tools \
  bash-completion \
  docker \
  docker-compose \
  cmus libmad \
  firefox \
  chromium \
  deadbeef \
  id3 \
  docker \
  terminator \
  imagemagick \
  calibre \
  gimp \
  inkscape \
  simple-scan \
  gtk3-print-backends \
  transmission-gtk \
  libreoffice-fresh \
  libreoffice-fresh-pl \
  parted \
  octave \
  jre8-openjdk \
  gnuplot \
  thunderbird \
  gparted \
  texlive-core \
  texlive-most \
  biber \
  pandoc \
  pandoc-citeproc \
  pandoc-crossref \
  qemu \
  libvirt virt-viewer virt-install \
  sway i3blocks acpi ttf-font-awesome grim slurp \
  virt-manager ebtables dnsmasq \
  exfat-utils \
  mpv \
  imv \
  zathura zathura-pdf-mupdf zathura-djvu zathura-ps \
  radicale khal khard todoman

set +v
