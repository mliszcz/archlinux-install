# archlinux-install
My personalized Arch Linux installation.

## Install Arch

```bash
./01-partitioning.sh /dev/sda 8G 40G
arch-chroot /mnt
cd ~/archlinux-install
./02-install.sh /dev/sda2 pinky
exit
umount /mnt/boot; umount /mnt/home; umount /mnt
reboot
useradd -m -G wheel -s /bin/bash michal; passwd michal
./03-gnome-desktop.sh
pacman -Syu --noconfirm open-vm-tools # for VM-based installation
reboot
```
