#!/usr/bin/env python
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import argparse
import subprocess


class Colors:  # pylint: disable=too-few-public-methods
    BOLD            = '\033[1m'   # noqa: E221
    BOLD_OFF        = '\033[22m'  # noqa: E221
    ITALIC          = '\033[3m'   # noqa: E221
    ITALIC_OFF      = '\033[23m'  # noqa: E221
    UNDERLINE       = '\033[4m'   # noqa: E221
    UNDERLINE_OFF   = '\033[24m'  # noqa: E221
    FG_RED          = '\033[31m'  # noqa: E221
    FG_GREEN        = '\033[32m'  # noqa: E221
    FG_YELLOW       = '\033[33m'  # noqa: E221
    FG_BLUE         = '\033[34m'  # noqa: E221
    FG_OFF          = '\033[39m'  # noqa: E221
    RESET           = '\033[0m'   # noqa: E221


def fg(color, text):  # pylint: disable=invalid-name
    return f"{color}{text}{Colors.FG_OFF}"


def bold(text):
    return f"{Colors.BOLD}{text}{Colors.BOLD_OFF}"


def get_installed_packages(command: list[str]) -> set[str]:
    query = subprocess.run(command, text=True, capture_output=True, check=True)
    return set(query.stdout.splitlines())


def read_packages_file(path: str) -> set[str]:
    with open(path, "r", encoding="utf8") as file:
        lines = (p.strip() for p in file.readlines())
        return {p for p in lines if p and not p.startswith("#")}


def process(pkg_lists_files: list[str], installed_cmd: str, explicit_cmd: str):
    installed = get_installed_packages(installed_cmd.split())
    explicit = get_installed_packages(explicit_cmd.split())

    for file in pkg_lists_files:
        requested = read_packages_file(file)
        explicit -= requested
        diff = requested - installed
        if not diff:
            print(fg(Colors.FG_GREEN, f"{bold(file)}: OK"))
        else:
            print(fg(Colors.FG_RED, f"{bold(file)}: MISSING"))
            for pkg in sorted(list(diff)):
                print(fg(Colors.FG_RED, f"  {pkg}"))

    if explicit:
        print(fg(Colors.FG_YELLOW, bold("UNTRACKED PACKAGES")))
        for pkg in sorted(list(explicit)):
            print(fg(Colors.FG_YELLOW, f"  {pkg}"))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("package lists", nargs="*")
    parser.add_argument("--installed-cmd", default="pacman -Qq")
    parser.add_argument("--explicit-cmd", default="pacman -Qqe")
    args = parser.parse_args()
    pkg_lists_files = getattr(args, "package lists")
    process(pkg_lists_files, args.installed_cmd, args.explicit_cmd)


if __name__ == "__main__":
    main()
