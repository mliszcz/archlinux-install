#!/bin/sh

# Running QEMU with UEFI requires a special firmware, provided by packages:
# (Arch): edk2-ovmf: /usr/share/edk2-ovmf/x64/OVMF_{CODE,VARS}.fd
# (Debian): ovmf: /usr/share/OVMF/OVMF_{CODE,VARS}.fd
# The VARS file needs to be writable, QEMU shall get a copy.

efi_code="/usr/share/edk2-ovmf/x64/OVMF_CODE.fd"
efi_vars="./OVMF_VARS.fd"

if [ "$#" -ne 1 ]; then
  echo "usage: $0 <qcow2 disk image>"
  echo "example: $0 build/packer-arch"
  exit 1
fi

exec /usr/bin/qemu-system-x86_64 \
  -m 1G \
  -cpu host \
  -smp $(nproc) \
  -machine type=pc,accel=kvm \
  -drive "if=pflash,format=raw,readonly=on,file=$efi_code" \
  -drive "if=pflash,format=raw,file=$efi_vars" \
  -drive "if=none,file=$1,id=drive0,cache=writeback,discard=ignore,format=qcow2" \
  -device virtio-scsi-pci,id=scsi0 \
  -device scsi-hd,bus=scsi0.0,drive=drive0 \
  -device virtio-net-pci,netdev=user.0 \
  -netdev user,id=user.0,hostfwd=tcp::10022-:22 \
  -snapshot
